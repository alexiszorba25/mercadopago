package com.spiraquis.alexis.mercadopagotest.view;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.spiraquis.alexis.mercadopagotest.R;
import com.spiraquis.alexis.mercadopagotest.model.MedioPago;
import com.spiraquis.alexis.mercadopagotest.presenter.MedioPagoAdapter;
import com.spiraquis.alexis.mercadopagotest.presenter.MedioPagoMvp;
import com.spiraquis.alexis.mercadopagotest.presenter.MedioPagoPresenter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MedioPagoActivity extends AppCompatActivity implements MedioPagoMvp.View{
    private ProgressBar progress;
    private MedioPagoAdapter mpAdapter;
    private MedioPagoPresenter mpPresenter;
    private LinearLayoutManager lLayout;
    private RecyclerView mpList;
    private String monto;
    HashMap<String, String> wsParams = new HashMap<>();
    private MedioPagoAdapter.MedioPagoItemListener mItemListener = new MedioPagoAdapter.MedioPagoItemListener() {
        @Override
        public void onMedioPagoClick(MedioPago clickedMP) {
            Intent intent = new Intent(MedioPagoActivity.this, BancoActivity.class);
            String medioPagoId = clickedMP.getId();
            intent.putExtra("medioPagoId", medioPagoId);
            intent.putExtra("monto", monto);
            startActivity(intent);
        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medio_pago);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        progress = findViewById(R.id.progress);
        mpList = findViewById(R.id.medio_pago_list);
        Intent intent = getIntent();
        monto = intent.getStringExtra("monto");
        Log.d("MP", "onCreate MedioPago: monto="+monto);
        mpAdapter = new MedioPagoAdapter(this, new ArrayList<MedioPago>(0),mItemListener);
        mpPresenter = new MedioPagoPresenter(this);

        lLayout = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        mpList.setLayoutManager(lLayout);
        mpList.setAdapter(mpAdapter);
        setParams();
        mpPresenter.loadMediosPago(wsParams);

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        if (item.getItemId() == android.R.id.home){
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    private void setParams(){
        wsParams.put("public_key", getString(R.string.publicKey));
    }
    @Override
    public void showMediosPago(List<MedioPago> mediosPago) {
        mpAdapter.replaceData(mediosPago);
    }

    @Override
    public void showLoadingState(boolean show) {
        if (show)
            progress.setVisibility(View.VISIBLE);
        else
            progress.setVisibility(View.GONE);
    }

    @Override
    public void showEmptyState(boolean show) {

    }

    @Override
    public void showError(String msg) {
        Toast.makeText(this,getString(R.string.errorReintentar),Toast.LENGTH_LONG).show();
    }
}
