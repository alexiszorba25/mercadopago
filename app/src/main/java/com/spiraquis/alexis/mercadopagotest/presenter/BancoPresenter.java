package com.spiraquis.alexis.mercadopagotest.presenter;

import com.spiraquis.alexis.mercadopagotest.data.BancoDataSource;
import com.spiraquis.alexis.mercadopagotest.data.IBancoDataSource;
import com.spiraquis.alexis.mercadopagotest.data.IMedioPagoDataSource;
import com.spiraquis.alexis.mercadopagotest.data.MedioPagoDataSource;
import com.spiraquis.alexis.mercadopagotest.model.Banco;
import com.spiraquis.alexis.mercadopagotest.model.MedioPago;

import java.util.List;
import java.util.Map;

public class BancoPresenter implements BancoMvp.Presenter{
    private final BancoMvp.View mView;

    public BancoPresenter(BancoMvp.View mBancoView){
        mView = mBancoView;
    }
    @Override
    public void loadBancos(Map<String, String> params) {
        mView.showLoadingState(true);
        BancoDataSource mDataSource = new BancoDataSource();
        mDataSource.getBancos(new IBancoDataSource.BancoServiceCallback() {
            @Override
            public void onLoaded(List<Banco> bancos) {
                if (bancos.isEmpty()) {//Hay tarjetas que no devuelven bancos
                    mView.showLoadingState(false);
                    mView.showEmptyState(true);
                }else {
                    mView.showLoadingState(false);
                    mView.showBanco(bancos);
                }
            }

            @Override
            public void onError(String error) {
                mView.showLoadingState(false);
                mView.showError(error);
            }
        },params);
    }
}
