package com.spiraquis.alexis.mercadopagotest.presenter;

import com.spiraquis.alexis.mercadopagotest.model.Cuota;

import java.util.List;
import java.util.Map;

import retrofit2.http.QueryMap;

public interface CuotaMvp {
    interface View {
        void showCuota(List<Cuota> cuotas);
        void showLoadingState(boolean show);

        void showEmptyState(boolean show);

        void showError(String msg);
    }
    interface Presenter {
        void loadCuotas(@QueryMap Map<String, String> params);
    }
}
