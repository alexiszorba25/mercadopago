package com.spiraquis.alexis.mercadopagotest.model;

import com.google.gson.annotations.SerializedName;

public class CuotaItem {
    @SerializedName("recommended_message")
    private String numCuota;

    public CuotaItem(String numCuota) {
        this.numCuota = numCuota;
    }

    public String getNumCuota() {
        return numCuota;
    }
}
