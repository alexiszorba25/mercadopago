package com.spiraquis.alexis.mercadopagotest.data;

import android.support.annotation.NonNull;
import android.util.Log;

import com.spiraquis.alexis.mercadopagotest.model.Banco;
import com.spiraquis.alexis.mercadopagotest.model.MedioPago;
import com.spiraquis.alexis.mercadopagotest.service.ErrorResponse;
import com.spiraquis.alexis.mercadopagotest.service.ServiceHelper;

import java.util.List;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BancoDataSource implements IBancoDataSource {

    @Override
    public void getBancos(final BancoServiceCallback callback, Map<String, String> params) {
        ServiceHelper.getInstance().getBancos(params).enqueue(new Callback<List<Banco>>() {
            @Override
            public void onResponse(@NonNull Call<List<Banco>> call, @NonNull Response<List<Banco>> response) {
                processResponse(response, callback);
            }

            @Override
            public void onFailure(@NonNull Call<List<Banco>> call, @NonNull Throwable t) {
                callback.onError(t.getMessage());
            }
        });
    }
    private void processResponse(Response<List<Banco>> response,
                                        BancoServiceCallback callback) {
        String error = "Falló la acción. Vuelva a intentarlo";
        ResponseBody errorBody = response.errorBody();

        if (errorBody != null) {
            try {
                if (errorBody.contentType().subtype().equals("json")) {
                    ErrorResponse errorResponse = ErrorResponse.fromErrorBody(errorBody);
                    error = errorResponse.getMessage();
                }
                callback.onError(error);
            }catch(Exception e){
                callback.onError(error);
            }
        }

        if (response.isSuccessful()) {
            callback.onLoaded(response.body());
        }


    }
}
