package com.spiraquis.alexis.mercadopagotest.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.spiraquis.alexis.mercadopagotest.model.Banco;
import com.spiraquis.alexis.mercadopagotest.model.Cuota;
import com.spiraquis.alexis.mercadopagotest.model.MedioPago;

import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.QueryMap;

public class ServiceHelper {

    private static OkHttpClient httpClient = new OkHttpClient();
    private static ServiceHelper instance = new ServiceHelper();
    private RestService service;

    private ServiceHelper() {

        Retrofit retrofit = createAdapter().build();
        service = retrofit.create(RestService.class);
    }

    public static ServiceHelper getInstance() {
        return instance;
    }

    private Retrofit.Builder createAdapter() {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.NONE);
        httpClient.newBuilder().addInterceptor(interceptor)
                .retryOnConnectionFailure(true)
                .connectTimeout(15, TimeUnit.SECONDS)
                .build();
        return new Retrofit.Builder()
                .baseUrl("https://api.mercadopago.com/v1/")
                .client(httpClient)
                .addConverterFactory(GsonConverterFactory.create(gson));
    }
    public Call<List<MedioPago>> getMediosPago(@QueryMap Map<String, String> params){
        return service.getMediosPago(params);
    }
    public Call<List<Banco>> getBancos(@QueryMap Map<String, String> params){
        return service.getBancos(params);
    }
    public Call<List<Cuota>> getCuotas(@QueryMap Map<String, String> params){
        return service.getCuotas(params);
    }
}
