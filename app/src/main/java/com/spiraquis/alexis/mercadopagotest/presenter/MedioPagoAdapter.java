package com.spiraquis.alexis.mercadopagotest.presenter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.spiraquis.alexis.mercadopagotest.R;
import com.spiraquis.alexis.mercadopagotest.model.MedioPago;
import com.spiraquis.alexis.mercadopagotest.utils.GlideApp;

import java.util.List;

import static android.support.v4.util.Preconditions.checkNotNull;

public class MedioPagoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context contexto;
    private List<MedioPago> mediosPago;
    private MedioPagoItemListener mMedioPagoItemListener;
    public MedioPagoAdapter(Context contexto, List<MedioPago> mediosPago, MedioPagoItemListener itemListener){
        this.contexto = contexto;
        this.mediosPago = mediosPago;
        mMedioPagoItemListener = itemListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        View view;
        RecyclerView.ViewHolder returnStatement;
        LayoutInflater inflater = LayoutInflater.from(context);
        view = inflater.inflate(R.layout.item_medio_pago, parent, false);
        returnStatement = new MedioPagoHolder(view, mMedioPagoItemListener);
        return returnStatement;
    }
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        MedioPago mpItem = getItem(position);
        MedioPagoHolder mpHolder = (MedioPagoHolder) holder;
        ImageView imageViewPath = mpHolder.mpImage;
        GlideApp.with(holder.itemView.getContext())
                .asBitmap()
                .load(mpItem.getImage())
                .into(imageViewPath);
        mpHolder.mpName.setText(mpItem.getName());
    }

    private void setList(List<MedioPago> mpList) {
        mediosPago = mpList;
    }

    public void replaceData(List<MedioPago> mpList) {
        setList(mpList);
        notifyDataSetChanged();
    }
    @Override
    public int getItemCount() {
        return mediosPago.size();
    }
    private MedioPago getItem(int position){
        return mediosPago.get(position);
    }
    public class MedioPagoHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        ImageView mpImage;
        TextView mpName;
        private MedioPagoItemListener mItemListener;

        MedioPagoHolder(View itemView, MedioPagoItemListener listener) {
            super(itemView);
            mpImage = itemView.findViewById(R.id.mpImage);
            mpName = itemView.findViewById(R.id.mpName);
            mItemListener = listener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mItemListener.onMedioPagoClick(getItem(getAdapterPosition()));
        }
    }
    public interface MedioPagoItemListener {
        void onMedioPagoClick(MedioPago clickedMP);
    }
}
