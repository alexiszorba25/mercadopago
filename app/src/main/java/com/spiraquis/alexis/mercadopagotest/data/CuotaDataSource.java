package com.spiraquis.alexis.mercadopagotest.data;

import android.support.annotation.NonNull;
import android.util.Log;

import com.spiraquis.alexis.mercadopagotest.model.Banco;
import com.spiraquis.alexis.mercadopagotest.model.Cuota;
import com.spiraquis.alexis.mercadopagotest.service.ErrorResponse;
import com.spiraquis.alexis.mercadopagotest.service.ServiceHelper;

import java.util.List;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CuotaDataSource implements ICuotaDataSource {

    @Override
    public void getCuotas(final CuotaServiceCallback callback, Map<String, String> params) {
        ServiceHelper.getInstance().getCuotas(params).enqueue(new Callback<List<Cuota>>() {
            @Override
            public void onResponse(@NonNull Call<List<Cuota>> call, @NonNull Response<List<Cuota>> response) {
                processResponse(response, callback);
            }

            @Override
            public void onFailure(@NonNull Call<List<Cuota>> call, @NonNull Throwable t) {
                callback.onError(t.getMessage());
            }
        });
    }
    private void processResponse(Response<List<Cuota>> response,
                                        CuotaServiceCallback callback) {
        String error = "Falló la acción. Vuelva a intentarlo";
        ResponseBody errorBody = response.errorBody();

        if (errorBody != null) {
            try {
                if (errorBody.contentType().subtype().equals("json")) {
                    ErrorResponse errorResponse = ErrorResponse.fromErrorBody(errorBody);
                    error = errorResponse.getMessage();
                }
                callback.onError(error);
            }catch(Exception e){
                callback.onError(error);
            }
        }

        if (response.isSuccessful()) {
            callback.onLoaded(response.body());
        }


    }
}
