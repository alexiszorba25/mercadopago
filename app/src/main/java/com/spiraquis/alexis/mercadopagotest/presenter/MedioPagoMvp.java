package com.spiraquis.alexis.mercadopagotest.presenter;

import com.spiraquis.alexis.mercadopagotest.model.MedioPago;

import java.util.List;
import java.util.Map;

import retrofit2.http.QueryMap;

public interface MedioPagoMvp {
    interface View {
        void showMediosPago(List<MedioPago> mediosPago);
        void showLoadingState(boolean show);

        void showEmptyState(boolean show);

        void showError(String msg);
    }
    interface Presenter {
        void loadMediosPago(@QueryMap Map<String, String> params);
    }
}
