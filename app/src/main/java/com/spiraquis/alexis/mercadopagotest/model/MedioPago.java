package com.spiraquis.alexis.mercadopagotest.model;

import com.google.gson.annotations.SerializedName;

public class MedioPago {
    @SerializedName("id")
    private String id;
    @SerializedName("name")
    private String name;
    @SerializedName("payment_type_id")
    private String type_id;
    @SerializedName("secure_thumbnail")
    private String image;

    public MedioPago(String id, String name, String type_id, String image) {
        this.id = id;
        this.name = name;
        this.type_id = type_id;
        this.image = image;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getType_id() {
        return type_id;
    }

    public String getImage() {
        return image;
    }
}
