package com.spiraquis.alexis.mercadopagotest.service;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import java.io.IOException;

import okhttp3.ResponseBody;

public class ErrorResponse {
    @SerializedName("message")
    private String mMessage;

    public String getMessage() {
        return mMessage;
    }

    public static ErrorResponse fromErrorBody(ResponseBody errorBody) {
        try {
            Log.d("PROFILE", "ErrorResponse: errorBody="+errorBody.string());
            return new Gson()
                    .fromJson(errorBody.string(), ErrorResponse.class);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return new ErrorResponse();
    }
}
