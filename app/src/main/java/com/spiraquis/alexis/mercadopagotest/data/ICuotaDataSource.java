package com.spiraquis.alexis.mercadopagotest.data;

import com.spiraquis.alexis.mercadopagotest.model.Banco;
import com.spiraquis.alexis.mercadopagotest.model.Cuota;

import java.util.List;
import java.util.Map;

public interface ICuotaDataSource {
    interface CuotaServiceCallback {

        void onLoaded(List<Cuota> cuotas);

        void onError(String error);

    }
    void getCuotas(CuotaServiceCallback callback, Map<String, String> params);
}
