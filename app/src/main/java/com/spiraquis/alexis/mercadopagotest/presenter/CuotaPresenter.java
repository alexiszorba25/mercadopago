package com.spiraquis.alexis.mercadopagotest.presenter;

import com.spiraquis.alexis.mercadopagotest.data.CuotaDataSource;
import com.spiraquis.alexis.mercadopagotest.data.ICuotaDataSource;
import com.spiraquis.alexis.mercadopagotest.model.Cuota;

import java.util.List;
import java.util.Map;

public class CuotaPresenter implements CuotaMvp.Presenter{
    private final CuotaMvp.View mView;

    public CuotaPresenter(CuotaMvp.View mCuotaView){
        mView = mCuotaView;
    }
    @Override
    public void loadCuotas(Map<String, String> params) {
        mView.showLoadingState(true);
        CuotaDataSource mDataSource = new CuotaDataSource();
        mDataSource.getCuotas(new ICuotaDataSource.CuotaServiceCallback() {
            @Override
            public void onLoaded(List<Cuota> cuotas) {
                mView.showLoadingState(false);
                mView.showCuota(cuotas);
            }

            @Override
            public void onError(String error) {
                mView.showLoadingState(false);
                mView.showError(error);
            }
        },params);
    }

}
