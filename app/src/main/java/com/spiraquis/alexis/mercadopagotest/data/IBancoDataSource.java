package com.spiraquis.alexis.mercadopagotest.data;

import com.spiraquis.alexis.mercadopagotest.model.Banco;
import com.spiraquis.alexis.mercadopagotest.model.MedioPago;

import java.util.List;
import java.util.Map;

public interface IBancoDataSource {
    interface BancoServiceCallback {

        void onLoaded(List<Banco> bancos);

        void onError(String error);

    }
    void getBancos(BancoServiceCallback callback, Map<String, String> params);
}
