package com.spiraquis.alexis.mercadopagotest.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.spiraquis.alexis.mercadopagotest.R;
import com.spiraquis.alexis.mercadopagotest.model.Banco;
import com.spiraquis.alexis.mercadopagotest.presenter.BancoAdapter;
import com.spiraquis.alexis.mercadopagotest.presenter.BancoMvp;
import com.spiraquis.alexis.mercadopagotest.presenter.BancoPresenter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class BancoActivity extends AppCompatActivity implements BancoMvp.View{
    private ProgressBar progress;
    private BancoAdapter bancoAdapter;
    private String monto;
    private String medioPagoId;
    HashMap<String, String> wsParams = new HashMap<>();
    private BancoAdapter.BancoItemListener mItemListener = new BancoAdapter.BancoItemListener() {
        @Override
        public void onBancoClick(Banco clickedBanco) {
            Intent intent = new Intent(BancoActivity.this, CuotaActivity.class);
            String bancoId = clickedBanco.getId();
            intent.putExtra("medioPagoId", medioPagoId);
            intent.putExtra("monto", monto);
            intent.putExtra("bancoId", bancoId);
            startActivity(intent);
        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_banco);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        progress = findViewById(R.id.progress);
        RecyclerView bancoList = findViewById(R.id.banco_list);
        Intent intent = getIntent();
        monto = intent.getStringExtra("monto");
        medioPagoId = intent.getStringExtra("medioPagoId");
        bancoAdapter = new BancoAdapter(this, new ArrayList<Banco>(0),mItemListener);
        BancoPresenter bancoPresenter = new BancoPresenter(this);

        LinearLayoutManager lLayout = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        bancoList.setLayoutManager(lLayout);
        bancoList.setAdapter(bancoAdapter);
        setParams();
        bancoPresenter.loadBancos(wsParams);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        if (item.getItemId() == android.R.id.home){
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    private void setParams(){
        wsParams.put("public_key", getString(R.string.publicKey));
        wsParams.put("payment_method_id",medioPagoId);
    }
    @Override
    public void showBanco(List<Banco> bancos) {
        bancoAdapter.replaceData(bancos);
    }

    @Override
    public void showLoadingState(boolean show) {
        if (show)
            progress.setVisibility(View.VISIBLE);
        else
            progress.setVisibility(View.GONE);
    }

    @Override
    public void showEmptyState(boolean show) {
        Intent intent = new Intent(BancoActivity.this, CuotaActivity.class);
        intent.putExtra("medioPagoId", medioPagoId);
        intent.putExtra("monto", monto);
        intent.putExtra("bancoId", "");
        startActivity(intent);
        finish();
    }

    @Override
    public void showError(String msg) {
        Toast.makeText(this,getString(R.string.errorReintentar),Toast.LENGTH_LONG).show();
    }
}
