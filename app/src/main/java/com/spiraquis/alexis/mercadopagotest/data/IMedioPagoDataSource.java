package com.spiraquis.alexis.mercadopagotest.data;

import com.spiraquis.alexis.mercadopagotest.model.MedioPago;

import java.util.List;
import java.util.Map;

public interface IMedioPagoDataSource {
    interface MedioPagoServiceCallback {

        void onLoaded(List<MedioPago> mediosPago);

        void onError(String error);

    }
    void getMediosPago(MedioPagoServiceCallback callback, Map<String, String> params);
}
