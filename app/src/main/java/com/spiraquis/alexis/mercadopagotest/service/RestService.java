package com.spiraquis.alexis.mercadopagotest.service;

import com.spiraquis.alexis.mercadopagotest.model.Banco;
import com.spiraquis.alexis.mercadopagotest.model.Cuota;
import com.spiraquis.alexis.mercadopagotest.model.MedioPago;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.QueryMap;

public interface RestService {
    @GET("payment_methods")
    Call<List<MedioPago>> getMediosPago(@QueryMap Map<String, String> params);

    @GET("payment_methods/card_issuers")
    Call<List<Banco>> getBancos(@QueryMap Map<String, String> params);

    @GET("payment_methods/installments")
    Call<List<Cuota>> getCuotas(@QueryMap Map<String, String> params);
}
