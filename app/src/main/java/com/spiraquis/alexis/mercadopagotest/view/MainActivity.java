package com.spiraquis.alexis.mercadopagotest.view;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.spiraquis.alexis.mercadopagotest.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button boton = findViewById(R.id.button);
        TextView successTitle = findViewById(R.id.successTitle);
        TextView success = findViewById(R.id.successMsg);
        Intent intent = getIntent();
        String montoTxt = intent.getStringExtra("monto");
        String medioPagoId = intent.getStringExtra("medioPagoId");
        String banco = intent.getStringExtra("banco");
        String cuota = intent.getStringExtra("cuota");
        if (montoTxt !=null){
            boton.setText(getString(R.string.title_activity_monto_new));
            successTitle.setText(getString(R.string.txt_success_title));
            success.setText(getString(R.string.txt_success, getString(R.string.txtMoneda),montoTxt, medioPagoId,banco, cuota));
        }
        boton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,MontoActivity.class);
                startActivity(intent);
            }
        });
    }
}
