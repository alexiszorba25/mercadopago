package com.spiraquis.alexis.mercadopagotest.presenter;

import com.spiraquis.alexis.mercadopagotest.model.Banco;
import com.spiraquis.alexis.mercadopagotest.model.MedioPago;

import java.util.List;
import java.util.Map;

import retrofit2.http.QueryMap;

public interface BancoMvp {
    interface View {
        void showBanco(List<Banco> bancos);
        void showLoadingState(boolean show);

        void showEmptyState(boolean show);

        void showError(String msg);
    }
    interface Presenter {
        void loadBancos(@QueryMap Map<String, String> params);
    }
}
