package com.spiraquis.alexis.mercadopagotest.presenter;

import com.spiraquis.alexis.mercadopagotest.data.MedioPagoDataSource;
import com.spiraquis.alexis.mercadopagotest.data.IMedioPagoDataSource;
import com.spiraquis.alexis.mercadopagotest.model.MedioPago;

import java.util.List;
import java.util.Map;

public class MedioPagoPresenter implements MedioPagoMvp.Presenter{
    private final MedioPagoMvp.View mView;

    public MedioPagoPresenter(MedioPagoMvp.View mpView){
        mView = mpView;
    }
    @Override
    public void loadMediosPago(Map<String, String> params) {
        mView.showLoadingState(true);
        MedioPagoDataSource mDataSource = new MedioPagoDataSource();
        mDataSource.getMediosPago(new IMedioPagoDataSource.MedioPagoServiceCallback() {
            @Override
            public void onLoaded(List<MedioPago> mediosPago) {
                mView.showLoadingState(false);
                mView.showMediosPago(mediosPago);
            }

            @Override
            public void onError(String error) {
                mView.showLoadingState(false);
                mView.showError(error);
            }
        },params);
    }
}
