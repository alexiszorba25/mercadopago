package com.spiraquis.alexis.mercadopagotest.presenter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.spiraquis.alexis.mercadopagotest.R;
import com.spiraquis.alexis.mercadopagotest.model.Banco;
import com.spiraquis.alexis.mercadopagotest.model.MedioPago;
import com.spiraquis.alexis.mercadopagotest.utils.GlideApp;

import java.util.List;

public class BancoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context contexto;
    private List<Banco> bancos;
    private BancoItemListener mBancoItemListener;
    public BancoAdapter(Context contexto, List<Banco> bancos, BancoItemListener itemListener){
        this.contexto = contexto;
        this.bancos = bancos;
        mBancoItemListener = itemListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        View view;
        RecyclerView.ViewHolder returnStatement;
        LayoutInflater inflater = LayoutInflater.from(context);
        view = inflater.inflate(R.layout.item_banco, parent, false);
        returnStatement = new BancoHolder(view, mBancoItemListener);
        return returnStatement;
    }
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        Banco bancoItem = getItem(position);
        BancoHolder bancoHolder = (BancoHolder) holder;
        ImageView imageViewPath = bancoHolder.bancoImage;
        GlideApp.with(holder.itemView.getContext())
                .asBitmap()
                .load(bancoItem.getImage())
                .into(imageViewPath);
        bancoHolder.bancoName.setText(bancoItem.getName());
    }

    private void setList(List<Banco> bancoList) {
        bancos = bancoList;
    }

    public void replaceData(List<Banco> bancoList) {
        setList(bancoList);
        notifyDataSetChanged();
    }
    @Override
    public int getItemCount() {
        return bancos.size();
    }
    private Banco getItem(int position){
        return bancos.get(position);
    }
    public class BancoHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        ImageView bancoImage;
        TextView bancoName;
        private BancoItemListener mItemListener;

        BancoHolder(View itemView, BancoItemListener listener) {
            super(itemView);
            bancoImage = itemView.findViewById(R.id.bancoImage);
            bancoName = itemView.findViewById(R.id.bancoName);
            mItemListener = listener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mItemListener.onBancoClick(getItem(getAdapterPosition()));
        }
    }
    public interface BancoItemListener {
        void onBancoClick(Banco clickedBanco);
    }
}
