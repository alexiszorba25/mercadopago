package com.spiraquis.alexis.mercadopagotest.model;

import com.google.gson.annotations.SerializedName;

public class Banco {
    @SerializedName("id")
    private String id;
    @SerializedName("name")
    private String name;
    @SerializedName("secure_thumbnail")
    private String image;

    public Banco(String id, String name, String image) {
        this.id = id;
        this.name = name;
        this.image = image;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getImage() {
        return image;
    }
}
