package com.spiraquis.alexis.mercadopagotest.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.spiraquis.alexis.mercadopagotest.R;
import com.spiraquis.alexis.mercadopagotest.model.Cuota;
import com.spiraquis.alexis.mercadopagotest.model.CuotaItem;
import com.spiraquis.alexis.mercadopagotest.presenter.CuotaMvp;
import com.spiraquis.alexis.mercadopagotest.presenter.CuotaPresenter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CuotaActivity extends AppCompatActivity implements CuotaMvp.View{
    private ProgressBar progress;
    private String monto;
    private String medioPagoId;
    private String bancoId;
    private String bancoName;
    private Spinner spinnerCuotas;
    private RadioGroup radioCuotas;
    HashMap<String, String> wsParams = new HashMap<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cuota);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        progress = findViewById(R.id.progress);
        radioCuotas = findViewById(R.id.radioCuotas);
        Button btnConfirmar = findViewById(R.id.finalizar);
        Intent intent = getIntent();
        monto = intent.getStringExtra("monto");
        medioPagoId = intent.getStringExtra("medioPagoId");
        bancoId = intent.getStringExtra("bancoId");
        CuotaPresenter cuotaPresenter = new CuotaPresenter(this);
        setParams();
        cuotaPresenter.loadCuotas(wsParams);
        radioCuotas.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

            }
        });
        btnConfirmar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (radioCuotas.getCheckedRadioButtonId()!=-1) {
                    Intent intent = new Intent(CuotaActivity.this, MainActivity.class);
                    intent.putExtra("monto", monto);
                    intent.putExtra("medioPagoId",medioPagoId);
                    intent.putExtra("banco",bancoName);
                    intent.putExtra("cuota",((RadioButton)findViewById(radioCuotas.getCheckedRadioButtonId())).getText().toString());
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                }else{
                    Toast.makeText(CuotaActivity.this, R.string.select_cuotas, Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        if (item.getItemId() == android.R.id.home){
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    private void setParams(){
        wsParams.put("public_key", getString(R.string.publicKey));
        wsParams.put("payment_method_id",medioPagoId);
        wsParams.put("issuer.id",bancoId);
        wsParams.put("amount",monto);
    }
    @Override
    public void showCuota(List<Cuota> cuotas) {
        bancoName = cuotas.get(0).getIssuer().getName(); //Recupero el Banco por las Tarjetas que no tienen
        fillCuotas(cuotas.get(0).getCuotas());
    }

    private void fillCuotas(ArrayList<CuotaItem> cuotas){
        for (CuotaItem cuota : cuotas) {
            RadioButton radioButton;
            LayoutInflater inflater = LayoutInflater.from(this);
            View v = inflater.inflate(R.layout.item_cuota,null);
            radioButton = v.findViewById(R.id.radio_button);
            radioButton.setText(cuota.getNumCuota());
            radioCuotas.addView(radioButton);
        }
    }
    @Override
    public void showLoadingState(boolean show) {
        if (show)
            progress.setVisibility(View.VISIBLE);
        else
            progress.setVisibility(View.GONE);
    }

    @Override
    public void showEmptyState(boolean show) {

    }

    @Override
    public void showError(String msg) {
        Log.d("MP", "showError: "+msg);
        Toast.makeText(this,getString(R.string.errorReintentar),Toast.LENGTH_LONG).show();
    }
}
