package com.spiraquis.alexis.mercadopagotest.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Cuota {
    @SerializedName("payment_method_id")
    private String payment_method_id;
    @SerializedName("payment_type_id")
    private String payment_type_id;
    @SerializedName("issuer")
    private Banco issuer;
    @SerializedName("payer_costs")
    private ArrayList<CuotaItem> cuotas;

    public Cuota(String payment_method_id, String payment_type_id, Banco issuer, ArrayList<CuotaItem> cuotas) {
        this.payment_method_id = payment_method_id;
        this.payment_type_id = payment_type_id;
        this.issuer = issuer;
        this.cuotas = cuotas;
    }

    public String getPayment_method_id() {
        return payment_method_id;
    }

    public String getPayment_type_id() {
        return payment_type_id;
    }

    public Banco getIssuer() {
        return issuer;
    }

    public ArrayList<CuotaItem> getCuotas() {
        return cuotas;
    }
}
