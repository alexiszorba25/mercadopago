package com.spiraquis.alexis.mercadopagotest.view;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.spiraquis.alexis.mercadopagotest.R;

public class MontoActivity extends AppCompatActivity {
    private TextView txtMonto;
    private String monto;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_monto);
        setTitle(R.string.title_activity_monto);
        txtMonto = findViewById(R.id.monto);
        Button btnContinuar = findViewById(R.id.continuar);
        btnContinuar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                monto = txtMonto.getText().toString();
                if (!monto.equals("")){
                    if (Double.parseDouble(monto)>getResources().getInteger(R.integer.min_amount)&&Double.parseDouble(monto)<=getResources().getInteger(R.integer.max_amount)) {
                        Intent intent = new Intent(MontoActivity.this, MedioPagoActivity.class);
                        intent.putExtra("monto", monto);
                        startActivity(intent);
                    }else{
                        Toast.makeText(MontoActivity.this, R.string.txt_limite_monto, Toast.LENGTH_LONG).show();
                    }
                }else{
                    Toast.makeText(MontoActivity.this, R.string.txt_ingrese_monto, Toast.LENGTH_LONG).show();
                }
            }
        });
    }
}
